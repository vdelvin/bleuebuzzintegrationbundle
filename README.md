BleuebuzzIntegrationBundle
==========================

Configuration du bundle dans un projet Symfony
-----------------------------------------------

    * ajouter au composer.json
        "require": {
            ....
            "bleuebuzz/integration-bundle": "dev-master",
            ....
        }

    * lancer la cmd "php composer.phar update"

    * ajouter au fichier app/AppKernel.php
        new Bleuebuzz\IntegrationBundle\BleuebuzzIntegrationBundle()

    * pour avoir accès au portfolio (/portfolio), ajouter au fichier app/config/config.yml
        bleuebuzz_integration:
          portfolio: true

    * pour étendre le layout du bundle
        {% extends "BleuebuzzIntegrationBundle::layout.html.twig" %}

    * pour étendre le form layout du bundle, ajouter au fichier app/config/config.yml
        twig:
          form:
            resources:
                - 'BleuebuzzIntegrationBundle:Form:form_div_layout.html.twig'
