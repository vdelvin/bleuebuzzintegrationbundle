<?php

namespace Bleuebuzz\IntegrationBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class BleuebuzzLanguageRequestListener
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($event->getRequestType() === HttpKernelInterface::MASTER_REQUEST)
        {
            // Retrieve manually chosen language if exist and is supported
            $chosenLanguage = FALSE;
            if (in_array($event->getRequest()->get('language'), $this->container->getParameter('bleuebuzz_integration.available_languages'))
            )
            {
                $chosenLanguage = $event->getRequest()->get('language');
                $event->getRequest()->getSession()->set('chosen-language', $chosenLanguage);
            }
            elseif(in_array($event->getRequest()->getSession()->get('chosen-language'), $this->container->getParameter('bleuebuzz_integration.available_languages'))
            )
            {
                $chosenLanguage = $event->getRequest()->getSession()->get('chosen-language');
            }

            // Retrieve navigator preferences if is supported
            $preferredLanguage = FALSE;
            $navigatorPreferences = explode('_', $event->getRequest()->getPreferredLanguage());
            if (in_array($navigatorPreferences[0], $this->container->getParameter('bleuebuzz_integration.available_languages')))
            {
                $preferredLanguage = $navigatorPreferences[0];
            }

            // If user has manually chosen a language
            if ($chosenLanguage)
            {
                $event->getRequest()->getSession()->set('_locale', $chosenLanguage);
//                $event->getRequest()->setDefaultLocale();
                $event->getRequest()->setLocale($chosenLanguage);
            }
            // If user has defined preferred language in navigator
            elseif ($preferredLanguage)
            {
                $event->getRequest()->getSession()->set('_locale', $preferredLanguage);
//                $event->getRequest()->setDefaultLocale($preferredLanguage);
                $event->getRequest()->setLocale($preferredLanguage);
            }
            // Else default language
            else
            {
                $event->getRequest()->getSession()->set('_locale', $event->getRequest()->getLocale());
//                $event->getRequest()->setDefaultLocale($event->getRequest()->getLocale());
            }
        }
    }
}