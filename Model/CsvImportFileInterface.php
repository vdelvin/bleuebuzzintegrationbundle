<?php

namespace Bleuebuzz\IntegrationBundle\Model;

interface CsvImportFileInterface
{
    public function setCsvArrayDatas(Array $datas);

    public function getCsvArrayDatas();

    public function addCsvLineErrors($lineInError);

    public function getCsvLineErrors();

    public function getAbsolutePath();

    public function addRow();
}
