<?php

namespace Bleuebuzz\IntegrationBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class BleuebuzzIntegrationExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        // Portfolio options
        $container->setParameter('bleuebuzz_integration.portfolio', $config['portfolio']);

        // FosUser optional twig services
        $container->setParameter('bleuebuzz_integration.use_fos_user', $config['use_fos_user']);
        $container->setParameter('bleuebuzz_integration.use_fos_router', $config['use_fos_router']);

        // Breadcrumb options
        $container->setParameter('bleuebuzz_integration.use_breadcrumb', $config['use_breadcrumb']);
        $container->setParameter('bleuebuzz_integration.breadcrumb_tpl', $config['breadcrumb_tpl']);

        // Language options
        $container->setParameter('bleuebuzz_integration.use_language', $config['use_language']);
        $container->setParameter('bleuebuzz_integration.available_languages', $config['available_languages']);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));

        // Load default services
        $loader->load('services.yml');

        // Load language service if necessary
        if ($container->getParameter('bleuebuzz_integration.use_language'))
        {
            $loader->load('language.yml');
        }
    }
}
