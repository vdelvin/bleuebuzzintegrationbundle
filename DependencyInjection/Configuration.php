<?php

namespace Bleuebuzz\IntegrationBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('bleuebuzz_integration');
        $rootNode
            ->children()
                ->booleanNode('portfolio')
                    ->treatNullLike(FALSE)
                ->end()
                ->booleanNode('use_fos_user')
                    ->treatNullLike(FALSE)
                ->end()
                ->booleanNode('use_fos_router')
                    ->treatNullLike(FALSE)
                ->end()
                ->booleanNode('use_breadcrumb')
                    ->treatNullLike(FALSE)
                ->end()
                ->scalarNode('breadcrumb_tpl')
                    ->treatNullLike('BleuebuzzIntegrationBundle:Render:bleuebuzzBreadcrumb.html.twig')
                ->end()
                ->booleanNode('use_language')
                    ->treatNullLike(FALSE)
                ->end()
                ->arrayNode('available_languages')
                    ->defaultValue(array('fr'))
                        ->prototype('scalar')
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
