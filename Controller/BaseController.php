<?php

namespace Bleuebuzz\IntegrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Base controller
 */
abstract class BaseController extends Controller
{
    /**
     * Breadcrump render block action with translated URLs
     *
     * @param Doctrine\ORM\Mapping\Entity $entity
     * @param string                      $route
     *
     * @return Response
     */
    public function bleuebuzzBreadcrumbAction($entity, $route)
    {
        $transRoute = $this->get('bleuebuzz.trans_route')->getDefaultBreadcrumbTrans($route);
        $routeOptions = $this->get('router')->getRouteCollection()->get($transRoute)->getOptions();

        $breadcrumbLinks = array();
        if ($routeOptions && isset($routeOptions[0]['breadcrumb']))
        {
            $breadcrumbOptions = $routeOptions[0]['breadcrumb'];

            foreach ($breadcrumbOptions as $linkConfigs)
            {
                $routeParams = array();
                if (is_array($linkConfigs))
                {
                    foreach ($linkConfigs as $route => $config)
                    {
                        $labelParam = array();
                        if (isset($config['label']))
                        {
                            if (is_array($config['label']))
                            {
                                foreach ($config['label'] as $entityRelationName => $property)
                                {
                                    $getMethod = 'get'.ucfirst($entityRelationName);
                                    $entityRelation = $entity->$getMethod();
                                    $getMethod = 'get'.ucfirst($property);
                                    $labelParam['__PLACE_HOLDER__'] = $entityRelation->$getMethod();
                                }
                            }
                            else
                            {
                                $getMethod = 'get'.ucfirst($config['label']);
                                $labelParam['__PLACE_HOLDER__'] = $entity->$getMethod();
                            }
                        }
                        $label = $this->get('translator')->trans($route, $labelParam, 'breadcrumb');
                        $newlink = array('label' => $label);

                        if (isset($config['params']))
                        {
                            if (is_array($config['params']))
                            {
                                foreach ($config['params'] as $param)
                                {
                                    if (is_array($param))
                                    {
                                        foreach ($param as $entityRelationName => $property)
                                        {
                                            $getMethod = 'get'.ucfirst($entityRelationName);
                                            $entityRelation = $entity->$getMethod();
                                            if (is_array($property))
                                            {
                                                foreach ($property as $key => $property)
                                                {
                                                    $getMethod = 'get'.ucfirst($property);
                                                    $routeParams = array_merge($routeParams, array($key => $entityRelation->$getMethod()));
                                                }
                                            }
                                            else
                                            {
                                                $getMethod = 'get'.ucfirst($property);
                                                $routeParams = array_merge($routeParams, array($entityRelationName => $entityRelation->$getMethod()));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        $getMethod = 'get'.ucfirst($param);
                                        $routeParams = array_merge($routeParams, array($param => $entity->$getMethod()));
                                    }
                                }
                            }
                            else
                            {
                                $getMethod = 'get'.ucfirst($config['params']);
                                $routeParams = array($config['params'] => $entity->$getMethod());
                            }
                        }
                    }
                }
                else
                {
                    $newlink = array('label' => $this->get('translator')->trans($linkConfigs, array(), 'breadcrumb'));
                    $route = $linkConfigs;
                }

                $newlink['route'] = $this->get('bleuebuzz.trans_route')->generateTransUrl($route, $routeParams);
                $breadcrumbLinks[] = $newlink;
            }
        }

        return $this->render($this->container->getParameter('bleuebuzz_integration.breadcrumb_tpl'),
            array('breadcrumbLinks' => $breadcrumbLinks)
        );
    }

    /**
     * Navigation block render action
     *
     * @param array  $navRoutes
     * @param string $active
     * @param array  $actionRoutes
     *
     * @return Response
     */
    public function bleuebuzzNavigationAction(array $navRoutes, $active, array $actionRoutes)
    {
        $nav = array();
        foreach ($navRoutes as $route => $params)
        {
            $nav[$route]['isActive'] = FALSE;
            if ($route === $this->get('bleuebuzz.trans_route')->retrieveGenericRoute($active))
            {
                $nav[$route]['isActive'] = TRUE;
            }
            $nav[$route]['link'] = $this->get('bleuebuzz.trans_route')->generateTransUrl($route, $params);
        }

        $actions = array();
        foreach ($actionRoutes as $route => $params)
        {
            if (isset($params['label']))
            {
                $actions[$route]['label'] = $params['label'];
                unset($params['label']);
            }
            $actions[$route]['isActive'] = FALSE;
            if ($route === $this->get('bleuebuzz.trans_route')->retrieveGenericRoute($active))
            {
                $actions[$route]['isActive'] = TRUE;
            }
            $actions[$route]['link'] = $this->get('bleuebuzz.trans_route')->generateTransUrl($route, $params);
        }

        return $this->render('BleuebuzzIntegrationBundle:Render:bleuebuzzNavigation.html.twig',
            array(
                'nav' => $nav,
                'actions' => $actions
            )
        );
    }

    /**
     * Language block render action
     *
     * @param string $route
     * @param array $params
     *
     * @return Response
     */
    public function bleuebuzzLanguageAction($route, array $params)
    {
        $referer = $this->get('bleuebuzz.trans_route')->generateTransUrl($route, $params);

        return $this->render('BleuebuzzIntegrationBundle:Render:bleuebuzzLanguage.html.twig',
            array(
                'availableLanguages' => $this->container->getParameter('bleuebuzz_integration.available_languages'),
                'referer' => $referer
            )
        );
    }
}