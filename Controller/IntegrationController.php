<?php

namespace Bleuebuzz\IntegrationBundle\Controller;

/**
 * Integration controller.
 */
class IntegrationController extends BaseController
{
    /**
     * Portfolio
     */
    public function portfolioAction()
    {
        if ($this->container->getParameter('bleuebuzz_integration.portfolio'))
        {
            return $this->render('BleuebuzzIntegrationBundle::portfolio.html.twig');
        }
        else
        {
            throw $this->createNotFoundException();
        }
    }
}
