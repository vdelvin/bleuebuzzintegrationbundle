/*jslint browser: true, indent: 2 */
/*global jQuery */
$(function($) {
  var link, pending, ajaxUrl, redirectUrl;
  link = $('#admin--switch-account');
  pending = false;
  redirectUrl = link.data('switch-redirect');
  ajaxUrl = '?_commercial-account=_exit';
  link.on('click', function (e) {
    e.preventDefault();
    if (!pending) {
    $.ajax ({
      url: ajaxUrl,
      statusCode: {
      200: function() {
        $(location).attr("href", redirectUrl);
        }
      }
    })
    }
  });
}(jQuery));