/*jslint browser: true, indent: 2 */
/*global jQuery */
(function ($) {
  'use strict';

  app.route = function (route, data) {
    return Routing.generate(route, data);
  }

}(jQuery));