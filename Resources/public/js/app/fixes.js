(function ($) {
  'use strict';


  // Apply class representing checked state
  var stateToClass = function (col) {
    col.each(function () {
      var el = $(this);
      el.toggleClass('is-checked', el.is(':checked'));
    });
  }

  // Listen all checkbox and radio changes
  $(document).on('change jsChange', 'input[type="checkbox"], input[type="radio"]', function (e) {
    var el = $(this);

    // If el is a radio, find all related ones
    if (el.is('[type="radio"]')) {
      el = el.add(el.closest('form, body').find('input[type="radio"][name="' + el.attr('name') + '"]'));
    }

    stateToClass(el);
  });

  stateToClass($('input[type="checkbox"], input[type="radio"]'));

}(jQuery));