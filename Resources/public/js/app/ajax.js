/*jslint browser: true, indent: 2 */
/*global jQuery */
(function ($, app) {
  'use strict';

  var pending, start, stop, getAttempts;

  pending = 0;
  $('body').append('<div class="xhr-loading">Chargement…</div>');

  getAttempts = function (attempts) {
    attempts = parseInt(attempts, 10);
    return isNaN(attempts) || attempts < 0 ? 3 : attempts;
  };

  start = function (el) {
    pending++;
    // Add loading class and message
    $('body').add(el).addClass('is-loading');
    //$(el).append('<div class="xhr-block-loading"></div>');

    // Block element modification
    el.add(el.find('*')).filter(':input:not(:disabled)')
      .attr('data-app--get',  true)
      .attr('disabled',       true);
  };

  stop = function (el) {
    pending--;
    if (!pending) {
      // Remove loading class and message
      $('body').add(el).removeClass('is-loading');
      //$(el).children('.xhr-block-loading').remove();

      // Unblock element
      el.add(el.find('*')).filter('[data-app--get]')
        .attr('data-app--get',  false)
        .attr('disabled',       false);
    }
  };

  app.get = function (obj, el, attempts) {
    // Make sure el is a jQuery object
    obj.type = 'GET';
    return app.ajax(obj, el, attempts);
  };

  app.post = function (obj, el, attempts) {
    // Make sure el is a jQuery object
    obj.type = 'POST';
    return app.ajax(obj, el, attempts);
  };

  app.ajax = function (obj, el, attempts) {
    var onError, onSuccess;

    el = $(el);
    attempts  = getAttempts(attempts);

    // Error
    onError   = obj.error;
    obj.error = function (e) {
      // Retry request if attempts left
      if (attempts) {
        attempts -= 1;
        $.ajax(obj);

      } else {
        stop(el);
        // Call original error
        if(typeof onError === 'function') {
          onError.apply(this, arguments);
        }
      }

      // Call original error if no attempt left
      if (!attempts && typeof onError === 'function') {
        onError.apply(this, arguments);
      }

    };

    // Success
    onSuccess = obj.success;
    obj.success = function (e) {
      stop(el);

      if (typeof onSuccess === 'function') {
        onSuccess.apply(this, arguments);
      }
    };

    start(el);
    return $.ajax(obj);
  };

  app._ajax = function (obj, attempts) {
    if (attempts) {
      return $.ajax(obj);
    }
  };

}(jQuery, app));