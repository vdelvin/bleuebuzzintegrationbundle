/*jslint browser: true, indent: 2 */
/*global jQuery */
(function ($) {
  'use strict';

  var app = {
    data: {
      // Filled in twig template
    }
  };

  window.app = app;

}(jQuery));