$(function() {
    var collectionHolder = $('.collectionWidgetForm');

    // Proto
    var collectionPrototype = '<li class="box is-outline">' + collectionHolder.attr('data-prototype') + '</li>';
    var newCollectionElementForm = function (name) {
        var element = $(collectionPrototype.replace(/__name__/g, name));
        addRemoveLink(element);
        return element;
    };
    var addRemoveLink = function (element) {
        $('<button class="button" type="button">Supprimer</button>').appendTo(element)
        .on('click', function() {
            element.remove();
        });
    };

    // New element
    $('.collectionWidgetForm').on('click', '#newElementLink', function(e) {
        console.log('test');
        e.preventDefault();
        if (collectionHolder.length === 0) {
            collectionHolder = $('ul.collectionWidgetForm');
            collectionPrototype = '<li>' + collectionHolder.attr('data-prototype') + '</li>';
        }
        addElementForm(collectionHolder);
    });

    // Server side elements
    collectionHolder.find('li').each(function() {
        addRemoveLink(this);
    });

    function addElementForm(collectionHolder) {
        var newFormLi = $(newCollectionElementForm(collectionHolder.children().length)).appendTo(collectionHolder);
    };
});