/*jslint browser: true, indent: 2 */
/*global jQuery, app */
$(function($, app) {
  if ($('.autocomplete').length > 0) {
    config = $('.autocomplete').data('autocomplete');
    urlParams = new Object();
    $.each(config.parameters, function(key, value) {
      urlParams[key] = value;
    });
    $('.autocomplete').autocomplete({ source: [] });
    url = app.route(config.route, urlParams);
    $('.autocomplete').autocomplete("option", "source", url);
  }
}(jQuery, app));
