/*jslint browser: true, indent: 2 */
/*global jQuery */
(function ($) {
  'use strict';

  $(document).on('click', '[data-get-href]', function (event) {
    app.get({
      url:      $(this).attr('data-get-href'),
      success:  function (data) {
        // Auto clicked link
        var a = $('<a></a>').appendTo('body')
          .attr({
            href: data,
            target: event.metaKey ? '_blank' : '_self'
          });
        a[0].click();
        a.remove();
      }
    });
  });

}(jQuery));