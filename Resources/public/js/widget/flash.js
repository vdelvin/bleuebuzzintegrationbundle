/*jslint browser: true, indent: 2 */
/*global jQuery */
(function ($) {
  'use strict';

  var close, delay;

  close = function (flash, toggle) {
    var d = parseFloat(flash.find('.flash-body').css('transition-duration').split(/s,[\s]+/img)[0]) * 1000;

    flash.delay(d).slideUp(d * 2 / 3, function () {
      flash.remove();
    });
  };

  delay = function (timer) {
    clearInterval(timer.interval);
    timer.interval = setInterval(function () {
      timer.duration -= 50;
      if (timer.duration <= 0) {
        timer.flash.find('.flash-state').trigger('click');
        clearInterval(timer.interval);
      }
    }, 50);
  };

  //close(flash, toggle);

  // Flash box
  $(document)
    .on('change', '.flash-state', function () {
      var
        toggle  = $(this),
        flash   = toggle.closest('.flash');
        close(flash, toggle);
    });

  $('.flash').each(function () {
    var timer, interval;

    timer     = {
      duration: 4000,
      flash:    $(this)
    };
    delay(timer);

    timer.flash.on('mouseenter', function () {
      clearInterval(timer.interval);
    }).on('mouseleave', function () {
      delay(timer);
    });
  });

}(jQuery));