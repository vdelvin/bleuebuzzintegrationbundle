$(function($, app) {
    'use strict';

  $('body').on('click', '[data-confirm-modal]', function(e) {
    e.preventDefault();
    var href, options;
    href = $(this).attr("href");
    options = $(this).data('confirm-modal');

    if (!options.modalElement) {
      options.modalElement = $('<div id="confirm-modal" title="'+options.title +'" class="confirm-dialog"></div>').html('<p>'+options.message+'</p>').appendTo('body');

      options.modalElement
        .dialog({
            modal:    true,
            buttons:  [
              {
                  text: options.ok,
                  attr: {'class':  'button'},
                  click: function() {
                      window.location.href = href;
                  }
              },
              {
                  text: options.cancel,
                  'class':  'button',
                  click: function() {
                      $(this).dialog( "close" );
                      $(this).remove();
                  }
              }
            ]
        });
    } else {
      options.modalElement.dialog('open');
    }
  });
});