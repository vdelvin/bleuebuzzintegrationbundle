/*jslint browser: true, indent: 2 */
/*global jQuery */
(function ($) {
  'use strict';

  /*
  $(document).on('change.app__widget__check-range', '[data-check-range]', function (event) {
    var el, selector;

    el        = $(this);
    selector  = el.attr('data-check-range');

    // Select range
    $(el.attr('data-check-range'))

      // Check range
      .prop('checked', el.prop('checked'))
      .trigger('jsChange');
  });

  $('[data-check-range]').trigger('change');*/

  $('[data-check-range]').each(function () {
    var el, targets, labels;
    // (un)check all
    el = $(this).on('click', function () {
      if (el.prop('checked')) {
        targets.not(':checked').trigger('click');
        labels.text('Tout fermer');
      } else {
        targets.filter(':checked').trigger('click');
        labels.text('Tout ouvrir');
      }
    });

    targets = $(el.attr('data-check-range')).on('change', function () {
      var checked = true;
      targets.each(function() {
        if (checked && !$(this).prop('checked')) {
          checked = false;
        }
      });
      el.prop('checked', checked);
    });

    labels = $('[for="' + el.attr('id') + '"]')
              .text(el.prop('checked') ? 'Tout fermer' : 'Tout ouvrir');
  });

}(jQuery));