/*jslint browser: true, indent: 2 */
/*global jQuery */
(function ($) {
  'use strict';

  var speed = 0;

  $(document)
    .on('change.app__widget__show-toggle', '[data-show-toggle]', function (event) {
      var el, target, usedClass;
  
      el        = $(this);
      target    = $(document).find(el.attr('data-show-toggle'));
      usedClass = el.attr('data-show-toggle-class');

      if (usedClass !== undefined) {
        usedClass = usedClass || 'is-opened';
      }

      if (el.is(':checked')) {
        if (usedClass) {
          target.addClass(usedClass);
        } else {
          target.stop(true, true).slideDown(speed);
        }
      } else {
        if (usedClass) {
          target.removeClass(usedClass);
        } else {
          target.stop(true, true).slideUp(speed);
        }
      }
    });

  $('[data-show-toggle]').trigger('change.app__widget__show-toggle');
  speed = 400;

}(jQuery));