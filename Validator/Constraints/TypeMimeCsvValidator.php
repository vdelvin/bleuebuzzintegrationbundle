<?php

namespace Bleuebuzz\IntegrationBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class TypeMimeCsvValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!is_null($value) && 'text/csv' !== $value->getClientMimeType())
        {
            $this->context->addViolation($constraint->message, array('%string%' => $value));
        }
    }
}