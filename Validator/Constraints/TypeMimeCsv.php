<?php

namespace Bleuebuzz\IntegrationBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class TypeMimeCsv extends Constraint
{
    public $message = 'invalid_csv_file';
}
