<?php

namespace Bleuebuzz\IntegrationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BleuebuzzIntegrationBundle extends Bundle
{
    public function getParent()
    {
        return 'TwigBundle';
    }
}
