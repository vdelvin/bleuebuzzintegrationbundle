<?php

namespace Bleuebuzz\IntegrationBundle\Services\Utils;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Description of BleuebuzzTransRoute
 *
 * @author vdelvin
 */
class BleuebuzzTransRoute
{
    const DEFAULT_BREADCRUMB_TRANS = 'fr';

    private $container;
    private $untranslatedRoutePrefixes = array('homepage', 'fos', 'portfolio');

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Generates an translated URL
     *
     * @param string         $route         The name of the route
     * @param mixed          $parameters    An array of parameters
     * @param Boolean|string $referenceType The type of reference (one of the constants in UrlGeneratorInterface)
     *
     * @return string The generated URL
     *
     * @see UrlGeneratorInterface
     */
    public function generateTransUrl($route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        $transRoute = $this->retrieveTransRoute($route);

        return $this->container->get('router')->generate($transRoute, $parameters, $referenceType);
    }

    /**
     * Retrieve route with adapted _locale parameter for translation
     *
     * @param string  $route
     *
     * @return string
     */
    private function retrieveTransRoute($route)
    {
        $routeDef = explode('_', $route);

        if ($this->container->getParameter('bleuebuzz_integration.use_language') && !in_array($routeDef[0], $this->untranslatedRoutePrefixes))
        {
            $paramsNbr = count($routeDef);
            if (!in_array($routeDef[$paramsNbr - 1], $this->container->getParameter('bleuebuzz_integration.available_languages')))
            {
                $route .= '_'.$this->container->get('request')->getSession()->get('_locale');
            }
        }

        return $route;
    }

    /**
     * Retrieve generic route
     *
     * @param string  $route
     *
     * @return string
     */
    public function retrieveGenericRoute($route)
    {
        $routeDef = explode('_', $route);
        if (!in_array($routeDef[0], $this->untranslatedRoutePrefixes)
            && in_array($routeDef[count($routeDef) - 1], $this->container->getParameter('bleuebuzz_integration.available_languages'))
        )
        {
            unset($routeDef[count($routeDef) - 1]);
            $route = implode('_', $routeDef);
        }

        return $route;
    }

    /**
     * Retrieve the default route for breadcrumb options
     *
     * @param string  $route
     *
     * @return string
     */
    public function getDefaultBreadcrumbTrans($route)
    {
        $routeDef = explode('_', $route);

        if ($this->container->getParameter('bleuebuzz_integration.use_language') && !in_array($routeDef[0], $this->untranslatedRoutePrefixes))
        {
            $paramsNbr = count($routeDef);
            if (in_array($routeDef[$paramsNbr - 1], $this->container->getParameter('bleuebuzz_integration.available_languages')))
            {
                if (self::DEFAULT_BREADCRUMB_TRANS === $routeDef[$paramsNbr - 1])
                {
                    $breadcrumbRouteOptions = $route;
                }
                else
                {
                    $routeDef[$paramsNbr - 1] = self::DEFAULT_BREADCRUMB_TRANS;
                    $breadcrumbRouteOptions = implode('_', $routeDef);
                }
            }
        }
        else
        {
            $breadcrumbRouteOptions = $route;
        }

        return $breadcrumbRouteOptions;
    }
}