<?php

namespace Bleuebuzz\IntegrationBundle\Services\Utils;

use Bleuebuzz\IntegrationBundle\Model\CsvImportFileInterface;

/**
 * Description of BleuebuzzCsvManipulator
 *
 * @author vdelvin
 */
class BleuebuzzCsvManipulator
{
    /**
     * Transform CSV file in an array
     *
     * @param CsvImportFileInterface $entity
     *
     * @return void
     */
    public function getArrayFromCsv(CsvImportFileInterface $entity)
    {
        $header = NULL;
        $datas = array();
        if (($handle = fopen($entity->getAbsolutePath(), 'r')) !== FALSE)
        {
            $i = 1;
            while (($row = fgetcsv($handle, 0, $entity->getSeparator())) !== FALSE)
            {
                if(!$header)
                {
                    $header = $row;
                }
                else
                {
                    $entity->addRow();
                    $newRow = @array_combine($header, $row);
                    if ($newRow)
                    {
                        $datas[$i] = $newRow;
                    }
                    else
                    {
                        $entity->addCsvLineErrors($i);
                    }
                }
                $i++;
            }
            fclose($handle);
        }

        $entity->setCsvArrayDatas($datas);
    }
}