<?php

namespace Bleuebuzz\IntegrationBundle\Services\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class BleuebuzzTransRouteExtension extends \Twig_Extension
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return array(
            'pathTrans' => new \Twig_Function_Method($this, 'pathTrans')
        );
    }

    public function pathTrans($route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->container->get('bleuebuzz.trans_route')->generateTransUrl($route, $parameters);
    }

    public function getName()
    {
        return 'bleuebuzz_twig_trans_route_extension';
    }
}