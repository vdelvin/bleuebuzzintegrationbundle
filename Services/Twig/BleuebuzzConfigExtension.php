<?php

namespace Bleuebuzz\IntegrationBundle\Services\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class BleuebuzzConfigExtension extends \Twig_Extension
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->container->get('twig')->addGlobal('use_fos_user', $container->getParameter('bleuebuzz_integration.use_fos_user'));
        $this->container->get('twig')->addGlobal('use_fos_router', $container->getParameter('bleuebuzz_integration.use_fos_router'));
        $this->container->get('twig')->addGlobal('use_breadcrumb', $container->getParameter('bleuebuzz_integration.use_breadcrumb'));
        $this->container->get('twig')->addGlobal('use_language', $container->getParameter('bleuebuzz_integration.use_language'));
    }

    public function getName()
    {
        return 'bleuebuzz_twig_config_extension';
    }
}